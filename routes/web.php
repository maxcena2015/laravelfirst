<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\Home\HomeController::class, 'index'])->name('home');

Route::resource('home_histories', App\Http\Controllers\Home\HistoryController::class);
Route::resource('admin_histories', App\Http\Controllers\Admin\HistoryController::class);
Route::resource('admin', App\Http\Controllers\Admin\AdminController::class)->middleware('admin');