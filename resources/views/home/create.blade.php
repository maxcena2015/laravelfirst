@extends('layouts.app')

@section('content')

<div class="container">
    <h3>Add History</h3>
    <form action="{{ route('home_histories.store') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-12">

                 <div class="form-group">
                    <label>Title</label>
                    <input value="{{ old('title') }}" 
                         type="text" class="form-control" name='title'>
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <label>Description</label>
                    <textarea
                        type="text" name="description" class="form-control">{{ old('description') }}</textarea>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <input value="{{ Auth::user()->id }}" 
                         type="hidden" class="form-control" name='user_id'>
                    <br>
                    <button class="btn btn-success">Submit</button>
                 </div>

            </div>
        </div>
    </form>
</div>

@endsection
