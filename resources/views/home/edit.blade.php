@extends('layouts.app')

@section('content')

<div class="container">
    <h3>Update History</h3>
    <form method="POST" action="{{ route('home_histories.update', $home_history) }}">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-md-12">

                 <div class="form-group">
                    <input value="{{ old('title', $home_history->title) }}" 
                        type="text" name='title' class="form-control">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <textarea 
                        type="text" name="description" class="form-control">{{ old('description', $home_history->description) }}</textarea>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <button class="btn btn-success">Submit</button>
                 </div>

            </div>
        </div>
    </form>
</div>

@endsection
