@extends('layouts.app')

@section('content')

<a type="button" class="btn btn-secondary" href="{{ route('home_histories.index') }}">Back to histories</a>
<div class="card m-auto" style="width: 18rem;">
	<ul class="list-group list-group-flush">
		<li class="list-group-item">ID: {{ $home_history->id }}</li>
		<li class="list-group-item">Title: {{ $home_history->title }}</li>
		<li class="list-group-item">Description: {{ $home_history->description }}</li>
		<li class="list-group-item">User name: {{ $home_history-> user -> name }}</li>
		<li class="list-group-item">Created at: {{ $home_history->created_at }}</li>
		<li class="list-group-item">Updated at: {{ $home_history->updated_at }}</li>
	</ul>
</div>

@endsection
