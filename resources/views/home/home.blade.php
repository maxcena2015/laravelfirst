@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
            <div class="container">

                @if ((Auth::user() -> is_admin) == 1) 

                <div class="row">
                    <button type="button" class="btn btn-primary mt-2 ml-auto mr-auto"><a href="{{ route('admin.index') }}" style="color: white;">Go to admin panel</a></button>
                </div>

                @endif

                <div class="container">
                    <h3 class="mt-2">Histories</h3>
                    <a href="{{ route('home_histories.create') }}" class="btn btn-success mb-2">Create</a>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <table class="table">
                                <thead>

                                    <tr>
                                        <th>ID</th>
                                        <th>title</th>
                                        <th style="width: 30%; word-break:break-all; word-wrap:break-word;">description</th>
                                        <th>user name</th>
                                    </tr>

                                </thead>

                                <tbody>
                                 @foreach($histories as $history)
                                    <tr>
                                        <td>{{ $history -> id }}</td>
                                        <td>{{ $history -> title }}</td>
                                        <td style="width: 30%; word-break:break-all; word-wrap:break-word;">{{ $history -> description }}</td>
                                        <td>{{ $history -> user -> name }}</td>
                                        <td>
                                            <a type="button" href="{{ route('home_histories.edit', $history) }}">Edit</a>
                                        </td>
                                        <td>
                                            <a type="button" href="{{ route('home_histories.show', $history) }}">Show</a>
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('home_histories.destroy', $history) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                 @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection


