@extends('layouts.master')

@section('content')

<div class="container">
    <h3 class="mt-2">Histories</h3>
    <a href="{{ route('admin_histories.create') }}" class="btn btn-success mb-2">Create</a>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table class="table">
                <thead>

                    <tr>
                        <th>ID</th>
                        <th>title</th>
                        <th style="width: 30%; word-break:break-all; word-wrap:break-word;">description</th>
                        <th>user name</th>
                    </tr>

                </thead>

                <tbody>
                 @foreach($histories as $history)
                    <tr>
                        <td>{{ $history -> id }}</td>
                        <td>{{ $history -> title }}</td>
                        <td style="width: 30%; word-break:break-all; word-wrap:break-word;">{{ $history -> description }}</td>
                        <td>{{ $history -> user -> name }}</td>
                        <td>
                            <a type="button" href="{{ route('admin_histories.edit', $history) }}">Edit</a>
                        </td>
                        <td>
                            <a type="button" href="{{ route('admin_histories.show', $history) }}">Show</a>
                        </td>
                        <td>
                            <form method="POST" action="{{ route('admin_histories.destroy', $history) }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                 @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection('content')