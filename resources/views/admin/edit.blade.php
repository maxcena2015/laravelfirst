@extends('layouts.master')

@section('content')

<div class="container">
    <h3>Update History</h3>
    <form method="POST" action="{{ route('admin_histories.update', $admin_history) }}">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-md-12">

                 <div class="form-group">
                    <input value="{{ old('title', $admin_history->title) }}" 
                        type="text" name='title' class="form-control">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <textarea 
                        type="text" name="description" class="form-control">{{ old('description', $admin_history->description) }}</textarea>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <button class="btn btn-success">Submit</button>
                 </div>

            </div>
        </div>
    </form>
</div>

@endsection
