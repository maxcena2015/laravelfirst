<div id="left-sidebar" class="sidebar">
    <a href="javascript:void(0);" class="menu_toggle"><i class="fa fa-angle-left"></i></a>
    <div class="navbar-brand">
        <a href="#"><img src="{{ asset('assets/images/icon.svg') }}" alt="Mooli Logo" class="img-fluid logo"><span>Mooli</span></a>
        <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="fa fa-close"></i></button>
    </div>
    <div class="sidebar-scroll">
        <div class="user-account">
            <div class="user_div">
                <img src="{{ asset('assets/images/user.png') }}" class="user-photo" alt="User Profile Picture">
            </div>
            <div class="dropdown">
                <span>Web Developer,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>Alan Green</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">
                    <li><a href="#"><i class="fa fa-user"></i>My Profile</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>Messages</a></li>
                    <li><a href="#"><i class="fa fa-gear"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-power-off"></i>Logout</a></li>
                </ul>
            </div>
        </div>  
        <nav id="left-sidebar-nav" class="sidebar-nav">
            <ul id="main-menu" class="metismenu animation-li-delay">
                <li class="header">Main</li>
                <li class="{{ Request::segment(1) === 'dashboard' ? 'active' : null }}"><a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                
                <li class="header">Apps</li>
                <li class="{{ Request::segment(2) === 'inbox' ? 'active' : null }}"><a href="#"><i class="fa fa-envelope"></i> <span>Email</span> <span class="badge badge-default mr-0">12</span></a></li>
                <li class="{{ Request::segment(2) === 'chat' ? 'active' : null }}"><a href="#"><i class="fa fa-comments"></i> <span>Chat</span></a></li>
                <li class="{{ Request::segment(2) === 'apps' ? 'active' : null }}"><a href="#"><i class="fa fa-calendar"></i> <span>Calendar</span></a></li>
                <li class="{{ Request::segment(2) === 'todolist' ? 'active' : null }}"><a href="#"><i class="fa fa-th-list"></i> <span>Todo List</span></a></li>
                <li class="{{ Request::segment(2) === 'filemanager' ? 'active' : null }}"><a href="#"><i class="fa fa-folder"></i> <span>File Manager</span></a></li>
                <li class="{{ Request::segment(2) === 'contacts' ? 'active' : null }}"><a href="#"><i class="fa fa-address-book"></i> <span>Contacts</span></a></li>                    
                <li class="{{ Request::segment(2) === 'scrumboard' ? 'active' : null }}"><a href="#"><i class="fa fa-tasks"></i> <span>Scrumboard</span></a></li>
                <li class="{{ Request::segment(2) === 'blog' ? 'active' : null }}"><a href="#"><i class="fa fa-globe"></i> <span>Blog</span></a></li>
                <li class="{{ Request::segment(2) === 'social' ? 'active' : null }}"><a href="#"><i class="fa fa-share-alt-square"></i> <span>Social</span></a></li>
                
                <li class="header">Vendors</li>
                <li class="{{ Request::segment(1) === 'elements' ? 'active' : null }}">
                    <a href="#uiElements" class="has-arrow"><i class="fa fa-diamond"></i><span>ui Elements</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'helperclass' ? 'active' : null }}"><a href="#">Helper Classes</a></li>
                        <li class="{{ Request::segment(2) === 'bootstrap' ? 'active' : null }}"><a href="#">Bootstrap UI</a></li>
                        <li class="{{ Request::segment(2) === 'typography' ? 'active' : null }}"><a href="#">Typography</a></li>
                        <li class="{{ Request::segment(2) === 'tabs' ? 'active' : null }}"><a href="#">Tabs</a></li>
                        <li class="{{ Request::segment(2) === 'buttons' ? 'active' : null }}"><a href="#">Buttons</a></li>
                        <li class="{{ Request::segment(2) === 'icons' ? 'active' : null }}"><a href="#">Icons</a></li>
                        <li class="{{ Request::segment(2) === 'notifications' ? 'active' : null }}"><a href="#">Notifications</a></li>
                        <li class="{{ Request::segment(2) === 'colors' ? 'active' : null }}"><a href="#">Colors</a></li>
                        <li class="{{ Request::segment(2) === 'dialogs' ? 'active' : null }}"><a href="#">Dialogs</a></li>
                        <li class="{{ Request::segment(2) === 'listgroup' ? 'active' : null }}"><a href="#">List Group</a></li>
                        <li class="{{ Request::segment(2) === 'mediaobject' ? 'active' : null }}"><a href="#">Media Object</a></li>
                        <li class="{{ Request::segment(2) === 'modals' ? 'active' : null }}"><a href="#">Modals</a></li>
                        <li class="{{ Request::segment(2) === 'nestable' ? 'active' : null }}"><a href="#">Nestable</a></li>
                        <li class="{{ Request::segment(2) === 'progressbars' ? 'active' : null }}"><a href="#">Progress Bars</a></li>
                        <li class="{{ Request::segment(2) === 'rangesliders' ? 'active' : null }}"><a href="#">Range Sliders</a></li>
                    </ul>
                </li>
                <li class="{{ Request::segment(1) === 'forms' ? 'active' : null }}">
                    <a href="#forms" class="has-arrow"><i class="fa fa-pencil"></i><span>Forms Elements</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'formsbasic' ? 'active' : null }}"><a href="#">Basic Elements</a></li>
                        <li class="{{ Request::segment(2) === 'advanced' ? 'active' : null }}"><a href="#">Advanced Elements</a></li>
                        <li class="{{ Request::segment(2) === 'validation' ? 'active' : null }}"><a href="#">Form Validation</a></li>
                        <li class="{{ Request::segment(2) === 'wizard' ? 'active' : null }}"><a href="#">Form Wizard</a></li>
                        <li class="{{ Request::segment(2) === 'dragdropupload' ? 'active' : null }}"><a href="#">Drag &amp; Drop Upload</a></li>
                        <li class="{{ Request::segment(2) === 'cropping' ? 'active' : null }}"><a href="#">Image Cropping</a></li>
                        <li class="{{ Request::segment(2) === 'summernote' ? 'active' : null }}"><a href="#">Summernote</a></li>
                        <li class="{{ Request::segment(2) === 'editors' ? 'active' : null }}"><a href="#">CKEditor</a></li>
                        <li class="{{ Request::segment(2) === 'markdown' ? 'active' : null }}"><a href="#">Markdown</a></li>
                    </ul>
                </li>
                <li class="{{ Request::segment(1) === 'tables' ? 'active' : null }}">
                    <a href="#Tables" class="has-arrow"><i class="fa fa-table"></i><span>Tables</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'normal' ? 'active' : null }}"><a href="#">Normal Tables</a></li>
                        <li class="{{ Request::segment(2) === 'datatable' ? 'active' : null }}"><a href="#">Jquery Datatables</a></li>
                        <li class="{{ Request::segment(2) === 'editable' ? 'active' : null }}"><a href="#">Editable Tables</a></li>
                        <li class="{{ Request::segment(2) === 'tablecolor' ? 'active' : null }}"><a href="#">Tables Color</a></li>
                        <li class="{{ Request::segment(2) === 'filter' ? 'active' : null }}"><a href="#">Table Filter</a></li>
                        <li class="{{ Request::segment(2) === 'dragger' ? 'active' : null }}"><a href="#">Table dragger</a></li>
                    </ul>
                </li>
                <li class="{{ Request::segment(1) === 'charts' ? 'active' : null }}">
                    <a href="#charts" class="has-arrow"><i class="fa fa-pie-chart"></i><span>Charts</span></a>
                    <ul>
                        <li class="{{ Request::segment(2) === 'apex' ? 'active' : null }}"><a href="#">Apex Charts</a></li>
                        <li class="{{ Request::segment(2) === 'c_chart' ? 'active' : null }}"><a href="#">C3 Charts</a></li>
                        <li class="{{ Request::segment(2) === 'morris' ? 'active' : null }}"><a href="#">Morris Charts</a></li>
                        <li class="{{ Request::segment(2) === 'flot' ? 'active' : null }}"><a href="#">Flot Charts</a></li>
                        <li class="{{ Request::segment(2) === 'chartjs' ? 'active' : null }}"><a href="#">ChartJS Charts</a></li>
                        <li class="{{ Request::segment(2) === 'knob' ? 'active' : null }}"><a href="#">Knob Charts</a></li>
                        <li class="{{ Request::segment(2) === 'sparkline' ? 'active' : null }}"><a href="#">Sparkline Charts</a></li>
                    </ul>
                </li>
                
                <li class="header">More Pages</li>
                <li class="{{ Request::segment(2) === 'widget' ? 'active' : null }}"><a href="#"><i class="fa fa-puzzle-piece"></i><span>Widgets</span></a></li>
                
                <li class="{{ Request::segment(1) === 'pages' ? 'active' : null }}">
                    <a href="#Pages" class="has-arrow"><i class="fa fa-folder"></i><span>Pages</span></a>
                    <ul>
                        <li><a href="#">Login</a></li>
                        <li><a href="#">Register</a></li>
                        <li><a href="#">Forgot Password</a></li>
                        <li><a href="#">Page 404</a></li>
                        <li class="{{ Request::segment(2) === 'blank' ? 'active' : null }}"><a href="#">Blank Page</a></li>
                        <li class="{{ Request::segment(2) === 'settings' ? 'active' : null }}"><a href="#">Settings</a></li>
                        <li class="{{ Request::segment(2) === 'searchresults' ? 'active' : null }}"><a href="#">Search Results</a></li>
                        <li class="{{ Request::segment(2) === 'profile' ? 'active' : null }}"><a href="#">Profile</a></li>
                        <li class="{{ Request::segment(2) === 'invoices' ? 'active' : null }}"><a href="#">Invoices</a></li>
                        <li class="{{ Request::segment(2) === 'gallery' ? 'active' : null }}"><a href="#">Image Gallery</a></li>
                        <li class="{{ Request::segment(2) === 'timeline' ? 'active' : null }}"><a href="#">Timeline</a></li>
                        <li class="{{ Request::segment(2) === 'pricing' ? 'active' : null }}"><a href="#">Pricing</a></li>
                    </ul>
                </li>
                <li class="{{ Request::segment(2) === 'vectormap' ? 'active' : null }}"><a href="#"><i class="fa fa-map"></i> <span>jVector Maps</span></a></li>

                <li class="extra_widget">
                    <div class="form-group">
                        <label class="d-block">Traffic this Month <span class="float-right">77%</span></label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="d-block">Server Load <span class="float-right">50%</span></label>
                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>     
    </div>
</div>