<div class="sticky-note">
        <a href="javascript:void(0);" class="right_note"><i class="fa fa-close"></i></a>
        <div class="form-group c_form_group">
            <label>Write your note here</label>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter here...">
                <div class="input-group-append"><button class="btn btn-dark btn-sm" type="button">Add</button></div>
            </div>
        </div>
        <div class="note-body">
            
        </div>
</div>