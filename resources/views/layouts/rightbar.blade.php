<div id="rightbar" class="rightbar">
    <div class="slim_scroll">
        <div class="chat_list">
            <form>
                <div class="input-group c_input_group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="in-magnifier"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Search...">
                </div>
            </form>
            <div class="body">
                <ul class="nav nav-tabs3 white mt-3 d-flex text-center">
                    <li class="nav-item flex-fill"><a class="nav-link active show" data-toggle="tab" href="#chat-Users">Chat</a></li>
                    <li class="nav-item flex-fill"><a class="nav-link" data-toggle="tab" href="#chat-Groups">Groups</a></li>
                    <li class="nav-item flex-fill"><a class="nav-link mr-0" data-toggle="tab" href="#chat-Contact">Contact</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane vivify fadeIn active show" id="chat-Users">
                        <ul class="right_chat list-unstyled mb-0 animation-li-delay">
                            
                        </ul>
                    </div>
                    <div class="tab-pane vivify fadeIn" id="chat-Groups">
                        <ul class="right_chat list-unstyled mb-0 animation-li-delay">
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>