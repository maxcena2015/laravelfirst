<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = [
    	'title', 'description', 'user_id'
    ];

    public function user() {
    	
    	//История принаджежит пользователю
    	return $this->belongsTo(User::class);

    }

}

