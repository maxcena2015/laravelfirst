<?php

namespace App\Http\Controllers\Home;

use App\Models\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\HistoryRequest;
use Auth;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if ((Auth::user() -> is_admin) == 1) {
            $histories = History::all();
        } else {
            $user_id = Auth::user() -> id;
            $results = History::with(['user']) -> where('user_id', $user_id);
            $histories = $results -> get();
        }
        return view('home.home', compact('histories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('home.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HistoryRequest $request)
    {
        History::create($request->all());

        return redirect()->route('home_histories.index')
                            ->with('success', 'History added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(History $home_history)
    {
        return view('home.show', compact('home_history'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(History $home_history)
    {
        return view('home.edit', compact('home_history'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HistoryRequest $request, History $home_history)
    {
        $home_history->update($request->all());
        return redirect()->route('home_histories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(History $home_history)
    {
        $home_history->delete();
        return redirect()->route('home_histories.index');
    }
}
