<?php

namespace App\Http\Controllers\Admin;

use App\Models\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
    	return redirect()->route('admin_histories.index');
    }
}
